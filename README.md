# Source to Image AMQ 7

Example on how to deploy AMQ 7 on OpenShift using Source to Image(s2i)

## How to Deploy

    oc import-image amq-broker:7.7 --from registry.redhat.io/amq7/amq-broker:7.7 --confirm -n openshift
    oc new-app amq-broker:7.7~https://gitlab.com/openshift-samples/s2i-amq7.git -e AMQ_USER=admin -e AMQ_PASSWORD=admin -l amq=s2i
